/*
 * L3GD20H.c
 *
 *  Created on: May 11, 2020
 *      Author: Tunahan ÖZKEZER
 */

#include "L3GD20H.h"

SPI_HandleTypeDef *spiI;
GPIO_TypeDef *csPin_PORT;
uint16_t csPin_Pin;
uint8_t dps_data;

uint8_t L3GD20H_init(SPI_HandleTypeDef *accSPI, GPIO_TypeDef *cs_GPIO_PORT, uint16_t cs_GPIO_Pin, uint16_t dps)
{
	spiI = accSPI;	/**/	csPin_PORT=cs_GPIO_PORT;	/**/	csPin_Pin=cs_GPIO_Pin;
	uint8_t recieve_data;
	if(dps==245) { dps_data=0; }
	else if (dps==500) {dps_data=1; }
	else if (dps==2000) {dps_data=2;}


	L3GD20H_CTRL1_Config(0x0,0x0, 0x1, 0x1, 0x1, 0x1);
	L3GD20H_CTRL2_Config(0x0, 0x0, 0x0, 0x00);
	L3GD20H_CTRL3_Config(0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0);
	L3GD20H_CTRL4_Config(0x0, 0x0, dps_data, 0x0, 0x0, 0x0);
	L3GD20H_CTRL5_Config(0X0, 0X0, 0X0, 0X0, 0X00, 0X00);


	L3GD20H_LOW_ODR_Config(0x0, 0x1, 0x0, 0x0);

	recieve_data = L3GD20H_Check_Who_Am_I();
	return recieve_data;

}

uint8_t L3GD20H_CTRL1_Config(uint8_t DR, uint8_t BW, uint8_t PD, uint8_t Zen, uint8_t Yen, uint8_t Xen)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=CTRL1;
	buffer[1]= (DR<<6)|(BW<<4)|(PD<<3)|(Zen<<2)|(Yen<<1)|Xen;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(CTRL1);
	return recieve_data;
}

uint8_t L3GD20H_CTRL2_Config(uint8_t EXTRen, uint8_t LVLen, uint8_t HPM, uint8_t HPCF)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=CTRL2;
	buffer[1]= (EXTRen<<7)|(LVLen<<6)|(HPM<<5)|HPCF;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(CTRL2);
	return recieve_data;
}

uint8_t L3GD20H_CTRL3_Config(uint8_t INT1_IG, uint8_t INT1_Boot, uint8_t  H_Lactive, uint8_t PP_OD, uint8_t INT2_DRDY, uint8_t INT2_FTH, uint8_t INT2_ORun , uint8_t INT2_Empty)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=CTRL3;
	buffer[1]= (INT1_IG<<7)|(INT1_Boot<<6)|(H_Lactive<<5)|(PP_OD<<4)|(INT2_DRDY<<3)|(INT2_FTH<<2)|(INT2_ORun<<1)|INT2_ORun;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(CTRL3);
	return recieve_data;
}


uint8_t L3GD20H_CTRL4_Config(uint8_t BDU, uint8_t BLE, uint8_t FS, uint8_t IMPen, uint8_t ST, uint8_t SIM)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=CTRL4;
	buffer[1]= (BDU<<7)|(BLE<<6)|(FS<<5)|(IMPen<<3)|(ST<<2)|SIM;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(CTRL4);
	return recieve_data;
}

uint8_t L3GD20H_CTRL5_Config(uint8_t BOOT, uint8_t FIFO_EN, uint8_t StopOnFTH, uint8_t HPen, uint8_t IG_Sel, uint8_t Out_Sel)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=CTRL5;
	buffer[1]= (BOOT<<7)|(FIFO_EN<<6)|(StopOnFTH<<5)|(HPen<<4)|(IG_Sel<<2)|Out_Sel;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(CTRL5);
	return recieve_data;
}

uint8_t L3GD20H_Reference(uint8_t Ref)
{
	uint8_t buffer[2];
	uint8_t recieve_data;

	buffer[0]=REFERENCE;
	buffer[1]= Ref;
	L3GD20H_SPI_Write(buffer, 2);

	recieve_data=L3GD20H_SPI_Read(REFERENCE);
	return recieve_data;
}

int8_t L3GD20H_OUT_TEMP()
{
	int8_t recieve_data;
	recieve_data=L3GD20H_SPI_Read(OUT_TEMP);
	return recieve_data;
}

uint8_t L3GD20H_GET_STATUS()
{
	uint8_t recieve_data;
	recieve_data=L3GD20H_SPI_Read(STATUS);
	return recieve_data;
}

int16_t L3GD20H_OUT_X_RAW()
{
	int16_t recieve_data, out_x_l, out_x_h;
	out_x_l=L3GD20H_SPI_Read(OUT_X_L);
	out_x_h=L3GD20H_SPI_Read(OUT_X_H);
	recieve_data=(out_x_h<<8)|out_x_l;
	return recieve_data;
}

int16_t L3GD20H_OUT_X()
{
	int16_t recieve_data;
	recieve_data=L3GD20H_OUT_X_RAW();
	if(dps_data==0)			{ recieve_data *=L3GD20_SENSITIVITY_245DPS; }
	else if(dps_data==1){ recieve_data *=L3GD20_SENSITIVITY_500DPS; }
	else 								{ recieve_data *=L3GD20_SENSITIVITY_2000DPS;}
	return recieve_data;
}

int16_t L3GD20H_OUT_Y_RAW()
{
	int16_t recieve_data, out_y_l, out_y_h;
	out_y_l=L3GD20H_SPI_Read(OUT_Y_L);
	out_y_h=L3GD20H_SPI_Read(OUT_Y_H);
	recieve_data=(out_y_h<<8)|out_y_l;
	return recieve_data;
}

int16_t L3GD20H_OUT_Y()
{
	int16_t recieve_data;
	recieve_data=L3GD20H_OUT_Y_RAW();
	if(dps_data==0)			{ recieve_data *=L3GD20_SENSITIVITY_245DPS; }
	else if(dps_data==1){ recieve_data *=L3GD20_SENSITIVITY_500DPS; }
	else 								{ recieve_data *=L3GD20_SENSITIVITY_2000DPS;}
	return recieve_data;
}

int16_t L3GD20H_OUT_Z_RAW()
{
	int16_t recieve_data, out_z_l, out_z_h;
	out_z_l=L3GD20H_SPI_Read(OUT_Z_L);
	out_z_h=L3GD20H_SPI_Read(OUT_Z_H);
	recieve_data=(out_z_h<<8)|out_z_l;
	return recieve_data;
}

int16_t L3GD20H_OUT_Z()
{
	int16_t recieve_data;
	recieve_data=L3GD20H_OUT_Z_RAW();
	if(dps_data==0)			{ recieve_data *=L3GD20_SENSITIVITY_245DPS; }
	else if(dps_data==1){ recieve_data *=L3GD20_SENSITIVITY_500DPS; }
	else 								{ recieve_data *=L3GD20_SENSITIVITY_2000DPS;}
	return recieve_data;
}


uint8_t L3GD20H_LOW_ODR_Config(uint8_t DDRDY_HL, uint8_t I2C_dis, uint8_t SW_RES, uint8_t Low_ODR)
{
	uint8_t buffer[2];
		uint8_t recieve_data;

		buffer[0]=LOW_ODR;
		buffer[1]= (DDRDY_HL<<5)|(I2C_dis<<3)|(SW_RES<<2)|Low_ODR;
		L3GD20H_SPI_Write(buffer, 2);

		recieve_data=L3GD20H_SPI_Read(LOW_ODR);
		return recieve_data;
}

//Hexadecimal 0xD4 Decimal 212 döndürmesi gerek
uint8_t L3GD20H_Check_Who_Am_I()
{
	uint8_t recieve_data;
	recieve_data=L3GD20H_SPI_Read(WHO_AM_I);
	return recieve_data;
}

void L3GD20H_SPI_Write(uint8_t *writeData, uint16_t size)
{
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, writeData, size, 10);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
}

uint8_t L3GD20H_SPI_Read(uint8_t readData)
{
	
	uint8_t recieving_data;
	uint8_t transmit_data = readData|0x80;
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(spiI, &transmit_data, 1, 10);
	HAL_SPI_Receive(spiI, &recieving_data, 1, 10);
	HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);

	return recieving_data;
}
