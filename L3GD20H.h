/*
 * L3GD20H.h
 *
 *  Created on: May 11, 2020
 *      Author: Tunahan ÖZKEZER
 */

#include <main.h>

#ifndef INC_L3GD20H_H_
#define INC_L3GD20H_H_
#endif /* INC_L3GD20H_H_ */
//REGISTER ADDRESS MAP

//WHO_AM_I Register
#define WHO_AM_I 0X0F

//CTRL Registers
#define CTRL1 0X20
#define CTRL2 0X21
#define CTRL3 0X22
#define CTRL4 0X23
#define CTRL5 0X24


#define REFERENCE 0X25
#define OUT_TEMP 0X26
#define STATUS 0X27


#define OUT_X_L 0x28
#define	OUT_X_H 0x29
#define OUT_Y_L 0x2A
#define OUT_Y_H 0X2B
#define OUT_Z_L 0X2C
#define OUT_Z_H 0X2D

#define FIFO_CTRL 0X2E
#define FIFO_SRC 0X2F

#define IG_CFG 0X30
#define IG_SRC 0X31
#define IG_THS_XH 0X32
#define IG_THS_XL 0X33
#define IG_THS_YH 0X34
#define IG_THS_YL 0X35
#define IG_THS_ZH 0X36
#define IG_THS_ZL 0X37
#define IG_DURATION 0X38

#define LOW_ODR 0X39

#define L3GD20_SENSITIVITY_245DPS  (0.00875F)      // Roughly 22/256 for fixed point match
#define L3GD20_SENSITIVITY_500DPS  (0.0175F)       // Roughly 45/256
#define L3GD20_SENSITIVITY_2000DPS (0.070F)        // Roughly 18/256
#define L3GD20_DPS_TO_RADS         (0.017453293F)  // degress/s to rad/s multiplier




/*
#define cs_LOW HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_RESET);
#define cs_HIGH HAL_GPIO_WritePin(csPin_PORT, csPin_Pin, GPIO_PIN_SET);
*/

uint8_t L3GD20H_init(SPI_HandleTypeDef *accSPI, GPIO_TypeDef *cs_GPIO_PORT, uint16_t cs_GPIO_Pin, uint16_t dps);
uint8_t L3GD20H_CTRL1_Config(uint8_t DR, uint8_t BW, uint8_t PD, uint8_t Zen, uint8_t Yen, uint8_t Xenw);
uint8_t L3GD20H_CTRL2_Config(uint8_t EXTRen, uint8_t LVLen, uint8_t HPM, uint8_t HPCF);
uint8_t L3GD20H_CTRL3_Config(uint8_t INT1_IG, uint8_t INT1_Boot, uint8_t  H_Lactive, uint8_t PP_OD, uint8_t INT2_DRDY, uint8_t INT2_FTH, uint8_t INT2_ORun , uint8_t INT2_Empty);
uint8_t L3GD20H_CTRL4_Config(uint8_t BDU, uint8_t BLE, uint8_t FS, uint8_t IMPen, uint8_t ST, uint8_t SIM);
uint8_t L3GD20H_CTRL5_Config(uint8_t BOOT, uint8_t FIFO_EN, uint8_t StopOnFTH, uint8_t HPen, uint8_t IG_Sel, uint8_t Out_Sel);
uint8_t L3GD20H_Reference(uint8_t Ref);
int8_t L3GD20H_OUT_TEMP();
uint8_t L3GD20H_GET_STATUS();
int16_t L3GD20H_OUT_X_RAW();
int16_t L3GD20H_OUT_X();
int16_t L3GD20H_OUT_Y_RAW();
int16_t L3GD20H_OUT_Y();
int16_t L3GD20H_OUT_Z_RAW();
int16_t L3GD20H_OUT_Z();
uint8_t L3GD20H_LOW_ODR_Config(uint8_t DDRDY_HL, uint8_t I2C_dis, uint8_t SW_RES, uint8_t Low_ODR);
uint8_t L3GD20H_Check_Who_Am_I();
void L3GD20H_SPI_Write(uint8_t *writeData, uint16_t size);
uint8_t L3GD20H_SPI_Read(uint8_t readData);
